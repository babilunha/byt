package p1;

public class Originator {

	private String state;
	

	public void setState(String state) {
		System.out.println("originator: setting state to: " + state);
		this.state = state;
	}

	public Memento createMemento() {
		System.out.println("originator: memento created");
		return new Memento(state);
	}

	public void restore(Memento m) {
		state = m.getState();
		System.out.println("originator: state after restoring: " + state);
	}

	public String getState() {
		return state;
	}
	
	
}
