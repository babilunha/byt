public class Matcher {
	public Matcher() {
	}
	/*
	 * bs: duplicated code
	 * ref: 
	 */
	public boolean match(int[] expected, int[] actual, int clipLimit, int delta) {
		
		int actualLenght = actual.length;

		// Clip "too-large" values
		for (int i = 0; i < actualLenght; i++)
			clipLimitCheck(actual, clipLimit, i); 

		// Check for length differences
		if (actualLenght != expected.length)
			return false;

		// Check that each entry within expected +/- delta
		for (int i = 0; i < actualLenght; i++)
			if (Math.abs(expected[i] - actual[i]) > delta) 
				return false;

		return true;
	}
	
	private void clipLimitCheck(int[] actual, int clipLimit, int i) {
		if (actual[i] > clipLimit)
			actual[i] = clipLimit;
	}
	
}