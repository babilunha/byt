import java.util.*;

public class Configuration {
	public int interval;

	public int duration;

	public int departure;

	/*
	 * bad smell: duplicated code, long method
	 * refactoring:  exctract method
	 */
	public void load(Properties props) throws ConfigurationException {
		
		interval = check(props, "interval");
		duration = superCheck(props, "duration");
		departure = superCheck(props, "departure");

	}

	private int superCheck(Properties props, String name) throws ConfigurationException {
		int tempValue = check(props, name);
		if ((tempValue % interval) != 0) {
			throw new ConfigurationException("duration % interval");
		}
		return tempValue;
	}

	private int check(Properties props, String name) throws ConfigurationException {
		String valueString;
		int value;
		valueString = props.getProperty(name);
		value = Integer.parseInt(valueString);
		if (valueString == null) {
			throw new ConfigurationException(name + " can't be null");
		}
		if (value <= 0) {
			throw new ConfigurationException(name + " can't be less than zero");
		}
		return value;
	}
}
