
// bs: Switch statement
// ref: chain of responsibility design method
public class Expression {

	private char op;

	private Expression left;

	private Expression right;

	private int constant;

	public Expression(int constant) {
		this.op = 'c';
		this.constant = constant;
	}

	public Expression(char op, Expression left, Expression right) {
		this.op = op;
		this.left = left;
		this.right = right;
	}

	public int evaluate() {

		Chain chainAdd = new Add();
		Chain chainSub = new Sub();
		Chain chainMul = new Mul();
		Chain chainDiv = new Div();
		Chain chainSingle = new SingleValue();

		chainAdd.setNextChain(chainSub);
		chainSub.setNextChain(chainMul);
		chainMul.setNextChain(chainDiv);
		chainDiv.setNextChain(chainSingle);

		int resultVal = chainAdd.calculate(this);

		return resultVal;
	}

	public char getOperation() {
		return op;
	}

	public int getLeft() {
		return left.getConst();
	}

	public int getRight() {
		return right.getConst();
	}

	public int getConst() {
		return constant;
	}
}
