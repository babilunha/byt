
public class Add implements Chain {

	private Chain nextInChain;

	public void setNextChain(Chain nextChain) {
		nextInChain = nextChain;
	}

	@Override
	public int calculate(Expression expr) {
		if (expr.getOperation() == '+') {
			return expr.getLeft() + expr.getRight();
		} else {
			return nextInChain.calculate(expr);
		}

	}
}