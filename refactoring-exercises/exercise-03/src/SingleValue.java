
public class SingleValue implements Chain {

	private Chain nextInChain;

	public void setNextChain(Chain nextChain) {
		nextInChain = nextChain;
	}

	@Override
	public int calculate(Expression expr) {

		return expr.getConst();

	}
}