package Assignment9;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CareTaker {

	  private List<Memento> mementoList = new ArrayList<Memento>();

	   public void add(Memento state){
	      mementoList.add(state);
	     serialize(state);
	   }
	   
	   public void serialize(Memento state) {
		   try {
		         FileOutputStream fileOut =
		         new FileOutputStream("StateFile.txt");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(String.valueOf((Date)state.getState()));
		         out.close();
		         fileOut.close();
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		   }
	
   public Memento get(int index){
	      return mementoList.get(index);
   }
}
